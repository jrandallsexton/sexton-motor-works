﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using smw.ai.assembly.dtos.Events;
using smw.ai.assembly.Infrastructure.DriveService;

using System;
using System.Threading;
using System.Threading.Tasks;
using smw.ai.drive.dtos.Commands;
using Timer = System.Threading.Timer;

namespace smw.ai.assembly.Features
{
    public class AssemblyLine : IHostedService, IDisposable
    {
        private int executionCount = 0;
        private readonly ILogger<AssemblyLine> _logger;
        private Timer _timer = null!;
        private readonly IDriveServiceClient _driveService;

        public AssemblyLine(ILogger<AssemblyLine> logger, IDriveServiceClient _driveServiceClient)
        {
            _logger = logger;
            _driveService = _driveServiceClient;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Assembly Line Service running.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(5));

            return Task.CompletedTask;
        }

        private async void DoWork(object? state)
        {
            var count = Interlocked.Increment(ref executionCount);

            _logger.LogInformation("Vehicle completed. Ready for self-drive. Total vehicle count: {Count}", count);

            var evt = new StartDriveCommand()
            {
                VehicleId = Guid.NewGuid().ToString()[..5]
            };

            try
            {
                var driveStarted = await _driveService.StartVehicleDrive(evt);
                _logger.LogInformation("DriveService reports 'OK'");
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "DriveService failed. Stopping production line!");
                await this.StopAsync(new CancellationToken(true));
            }
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Assembly Line Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}