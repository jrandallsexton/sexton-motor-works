﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using smw.ai.assembly.Infrastructure.DriveService;
using smw.ai.common.Infrastructure;

using System;

namespace smw.ai.assembly
{
    public static class DependencyInjection
    {
        public static IServiceCollection ConfigureHttpClients(this IServiceCollection services, IConfiguration config)
        {
            services.AddHttpClient(HttpClients.DriveService, client =>
            {
                var url = config["Services:DriveServiceUri"];
                var uri = new Uri(url);
                client.BaseAddress = uri;
            });

            return services;
        }

        public static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddTransient<IDriveServiceClient, DriveServiceClient>();

            return services;
        }
    }
}