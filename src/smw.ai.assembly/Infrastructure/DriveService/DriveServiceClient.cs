﻿using Microsoft.Extensions.Logging;

using smw.ai.common.ExtensionMethods;
using smw.ai.common.Infrastructure;
using smw.ai.drive.dtos.Commands;

using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace smw.ai.assembly.Infrastructure.DriveService
{
    public class DriveServiceClient : IDriveServiceClient
    {
        private readonly ILogger<DriveServiceClient> _logger;
        private readonly HttpClient _httpClient;

        public DriveServiceClient(ILogger<DriveServiceClient> logger, IHttpClientFactory clientFactory)
        {
            _logger = logger;
            _httpClient = clientFactory.CreateClient(HttpClients.DriveService);
        }

        public async Task<StartDriveResponse> StartVehicleDrive(StartDriveCommand command)
        {
            var content = new StringContent(command.ToJson(), Encoding.UTF8, "application/json");

            try
            {
                var response = await _httpClient.PostAsync("/api/drive", content);
                response.EnsureSuccessStatusCode();

                return await response.GetAsT<StartDriveResponse>();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error occurred in: {nameof(DriveServiceClient)}.{nameof(StartVehicleDrive)}");
                throw;
            }
        }
    }
}