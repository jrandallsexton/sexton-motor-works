﻿using smw.ai.drive.dtos.Commands;

using System.Threading.Tasks;

namespace smw.ai.assembly.Infrastructure.DriveService
{
    public interface IDriveServiceClient
    {
        Task<StartDriveResponse> StartVehicleDrive(StartDriveCommand command);
    }
}