﻿using System.ComponentModel.DataAnnotations;

namespace smw.ai.common.Data
{
    public interface IEntity
    {
        Guid Id { get; set; }
        DateTime CreatedUtc { get; set; }
        DateTime? ModifiedUtc { get; set; }
        Guid CreatedBy { get; set; }
        Guid? ModifiedBy { get; set; }
    }

    public abstract class EntityBase : IEntity
    {
        [Key]
        public Guid Id { get; set; }

        public DateTime CreatedUtc { get; set; } = DateTime.UtcNow;

        public DateTime? ModifiedUtc { get; set; }

        public Guid CreatedBy { get; set; }

        public Guid? ModifiedBy { get; set; }

        public DateTime LastModified => ModifiedUtc ?? CreatedUtc;
    }
}