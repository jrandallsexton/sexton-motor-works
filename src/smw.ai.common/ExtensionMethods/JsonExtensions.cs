﻿using Newtonsoft.Json;

namespace smw.ai.common.ExtensionMethods
{
    public static class JsonExtensions
    {
        public static T FromJson<T>(this string json, JsonSerializerSettings serializerSettings = null)
            where T : class
        {
            if (string.IsNullOrEmpty(json))
                return default;
            return JsonConvert.DeserializeObject<T>(json, serializerSettings);
        }

        public static string ToJson(this object obj, JsonSerializerSettings serializerSettings = null)
        {
            serializerSettings ??= new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            return JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None, serializerSettings);
        }
    }
}