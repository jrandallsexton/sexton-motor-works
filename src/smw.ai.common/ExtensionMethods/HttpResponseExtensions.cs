﻿namespace smw.ai.common.ExtensionMethods
{
    public static class HttpResponseExtensions
    {
        public static async Task<T> GetAsT<T>(this HttpResponseMessage httpResponse) where T : class
        {
            var responseData = await httpResponse.Content.ReadAsStringAsync();
            return responseData.FromJson<T>();
        }
    }
}