﻿using MediatR;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using smw.ai.common.Infrastructure;
using smw.ai.drive.Infrastructure.ParkService;

using System;
using System.Reflection;

namespace smw.ai.drive
{
    public static class DependencyInjection
    {
        public static IServiceCollection ConfigureHttpClients(this IServiceCollection services, IConfiguration config)
        {
            services.AddHttpClient(HttpClients.ParkService, client =>
            {
                var url = config["Services:ParkServiceUri"];
                var uri = new Uri(url);
                client.BaseAddress = uri;
            });

            return services;
        }

        public static IServiceCollection ConfigureMediatR(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            return services;
        }

        public static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddTransient<IParkServiceClient, ParkServiceClient>();

            return services;
        }
    }
}