﻿using MediatR;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using smw.ai.drive.dtos.Commands;
using smw.ai.drive.Features.Drive;

using System.Threading.Tasks;

namespace smw.ai.drive.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DriveController : ControllerBase
    {
        private readonly ILogger<DriveController> _logger;
        private readonly ISender _mediator;

        public DriveController(ILogger<DriveController> logger, ISender mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> StartDrive([FromBody] StartDriveCommand command)
        {
            _logger.LogInformation($"DriveService handling VehicleId: {command.VehicleId}");
            return Ok(await _mediator.Send(new StartDrive.Command(command)));
        }
    }
}