﻿using MediatR;

using Microsoft.Extensions.Logging;

using smw.ai.common.ExtensionMethods;
using smw.ai.common.Mapping;
using smw.ai.drive.dtos.Commands;
using smw.ai.drive.Infrastructure.ParkService;
using smw.ai.parking.dtos.Commands;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace smw.ai.drive.Features.Drive
{
    public class StartDrive
    {
        public class Command : StartDriveCommand, IRequest<Dto>
        {
            public Command(StartDriveCommand command)
            {
                VehicleId = command.VehicleId;
            }
        }

        public class Dto : StartDriveResponse, IMapFrom<StartDriveResponse>
        {
        }

        public class Handler : IRequestHandler<Command, Dto>
        {
            private readonly ILogger<StartDrive> _logger;
            private readonly IParkServiceClient _parkService;

            public Handler(ILogger<StartDrive> logger, IParkServiceClient parkService)
            {
                _logger = logger;
                _parkService = parkService;
            }

            public async Task<Dto> Handle(Command command, CancellationToken cancellationToken)
            {
                _logger.LogInformation("Handler began with: {command}", command.ToJson());

                var parkCommand = new ReserveParkingSpotCommand()
                {
                    CreatedUtc = DateTime.UtcNow,
                    VehicleId = command.VehicleId
                };

                await _parkService.RequestParkingSpot(parkCommand);

                return new Dto
                {
                    CreatedUtc = DateTime.UtcNow
                };
            }
        }
    }
}