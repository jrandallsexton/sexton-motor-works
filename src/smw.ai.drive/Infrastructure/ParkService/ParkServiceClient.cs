﻿using Microsoft.Extensions.Logging;

using smw.ai.common.ExtensionMethods;
using smw.ai.common.Infrastructure;
using smw.ai.parking.dtos.Commands;

using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace smw.ai.drive.Infrastructure.ParkService
{
    public class ParkServiceClient : IParkServiceClient
    {
        private readonly ILogger<ParkServiceClient> _logger;
        private readonly HttpClient _httpClient;

        public ParkServiceClient(ILogger<ParkServiceClient> logger, IHttpClientFactory clientFactory)
        {
            _logger = logger;
            _httpClient = clientFactory.CreateClient(HttpClients.ParkService);
        }

        public async Task<ReserveParkingSpotResponse> RequestParkingSpot(ReserveParkingSpotCommand command)
        {
            var content = new StringContent(command.ToJson(), Encoding.UTF8, "application/json");

            try
            {
                var response = await _httpClient.PostAsync("/api/parking", content);
                response.EnsureSuccessStatusCode();

                return await response.GetAsT<ReserveParkingSpotResponse>();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error occurred in: {nameof(ParkServiceClient)}.{nameof(RequestParkingSpot)}");
                throw;
            }
        }
    }
}