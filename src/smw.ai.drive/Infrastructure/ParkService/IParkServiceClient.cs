﻿using smw.ai.parking.dtos.Commands;

using System.Threading.Tasks;

namespace smw.ai.drive.Infrastructure.ParkService
{
    public interface IParkServiceClient
    {
        Task<ReserveParkingSpotResponse> RequestParkingSpot(ReserveParkingSpotCommand command);
    }
}