﻿using System;
using System.Collections.Generic;
using System.Text;

namespace smw.ai.drive.dtos.Events
{
    public class VehicleDriveStarted
    {
        public string VehicleId { get; set; }
        public DateTime DriveStartedUtc { get; set; }
    }
}