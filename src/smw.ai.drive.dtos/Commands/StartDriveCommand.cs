﻿using System;

namespace smw.ai.drive.dtos.Commands
{
    public class StartDriveCommand
    {
        public string VehicleId { get; set; }
    }

    public class StartDriveResponse
    {
        public DateTime CreatedUtc { get; set; }
    }
}