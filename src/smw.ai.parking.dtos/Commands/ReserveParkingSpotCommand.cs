﻿using System;

namespace smw.ai.parking.dtos.Commands
{
    public class ReserveParkingSpotCommand
    {
        public string VehicleId { get; set; }
        public DateTime CreatedUtc { get; set; }
    }

    public class ReserveParkingSpotResponse
    {
        public Guid ParkingSpotId { get; set; }
        public string ParkingSpotName { get; set; }
        public DateTime CreatedUtc { get; set; }
        public string VehicleId { get; set; }
    }
}