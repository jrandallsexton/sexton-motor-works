﻿using System;
using System.Collections.Generic;
using System.Text;

namespace smw.ai.assembly.dtos.Events
{
    public class VehicleCompleted
    {
        public string VehicleId { get; set; }
        public DateTime CompletedUtc { get; set; }
    }
}