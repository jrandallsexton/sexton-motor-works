﻿using Microsoft.EntityFrameworkCore;

using smw.ai.parking.Models;

namespace smw.ai.parking.Data
{
    public class ParkingDataContext : DbContext
    {
        public DbSet<ParkingLot> Lots { get; set; }
        public DbSet<ParkingLotRow> Rows { get; set; }
        public DbSet<ParkingSpace> Spaces { get; set; }

        public ParkingDataContext(DbContextOptions<ParkingDataContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ParkingLot.EntityConfiguration).Assembly);
        }
    }
}