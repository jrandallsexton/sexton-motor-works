﻿using MediatR;

using Microsoft.Extensions.DependencyInjection;

using System.Reflection;

namespace smw.ai.parking
{
    public static class DependencyInjection
    {
        public static IServiceCollection ConfigureMediatR(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            return services;
        }
    }
}