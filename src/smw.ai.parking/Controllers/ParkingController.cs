﻿using MediatR;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using smw.ai.parking.dtos.Commands;
using smw.ai.parking.Features.Parking;

using System.Threading.Tasks;

namespace smw.ai.parking.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        private readonly ILogger<ParkingController> _logger;
        private readonly ISender _mediator;

        public ParkingController(ILogger<ParkingController> logger, ISender mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> ReserveParkingSpot([FromBody] ReserveParkingSpotCommand command)
        {
            _logger.LogInformation($"ParkingController handling VehicleId: {command.VehicleId}");

            return Ok(await _mediator.Send(new ReserveParkingSpot.Command(command)));
        }
    }
}