﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using smw.ai.common.Data;

using System;

namespace smw.ai.parking.Models
{
    public class ParkingSpace : EntityBase
    {
        public Guid ParkingLotRowId { get; set; }

        public string Name { get; set; }

        public bool IsOccupied { get; set; }

        public string VehicleId { get; set; }

        public class EntityConfiguration : IEntityTypeConfiguration<ParkingSpace>
        {
            public void Configure(EntityTypeBuilder<ParkingSpace> builder)
            {
                builder.ToTable("Space");
                builder.HasKey(t => t.Id);
            }
        }
    }
}