﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using smw.ai.common.Data;

using System;
using System.Collections.Generic;

namespace smw.ai.parking.Models
{
    public class ParkingLotRow : EntityBase
    {
        public Guid ParkingLotId { get; set; }

        public string Name { get; set; }

        public List<ParkingSpace> Spaces { get; set; }

        public class EntityConfiguration : IEntityTypeConfiguration<ParkingLotRow>
        {
            public void Configure(EntityTypeBuilder<ParkingLotRow> builder)
            {
                builder.ToTable("Row");
                builder.HasKey(t => t.Id);
            }
        }
    }
}