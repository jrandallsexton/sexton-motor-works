﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using smw.ai.common.Data;

using System.Collections.Generic;

namespace smw.ai.parking.Models
{
    public class ParkingLot : EntityBase
    {
        public string Name { get; set; }

        public List<ParkingLotRow> Rows { get; set; }

        public class EntityConfiguration : IEntityTypeConfiguration<ParkingLot>
        {
            public void Configure(EntityTypeBuilder<ParkingLot> builder)
            {
                builder.ToTable("Lot");
                builder.HasKey(t => t.Id);
            }
        }
    }
}