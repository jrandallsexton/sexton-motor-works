﻿using FluentValidation;

using MediatR;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using smw.ai.common.ExtensionMethods;
using smw.ai.common.Mapping;
using smw.ai.parking.Data;
using smw.ai.parking.dtos.Commands;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace smw.ai.parking.Features.Parking
{
    public class ReserveParkingSpot
    {
        public class Command : ReserveParkingSpotCommand, IRequest<Dto>
        {
            public Command(ReserveParkingSpotCommand command)
            {
                VehicleId = command.VehicleId;
                CreatedUtc = command.CreatedUtc;
            }
        }

        public class Dto : ReserveParkingSpotResponse, IMapFrom<ReserveParkingSpotResponse> { }

        public class Validator : AbstractValidator<Command>
        {
            public Validator()
            {
                RuleFor(x => x.VehicleId).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Command, Dto>
        {
            private readonly ILogger<ReserveParkingSpot> _logger;
            private readonly ParkingDataContext _dbContext;

            public Handler(ILogger<ReserveParkingSpot> logger, ParkingDataContext dbContext)
            {
                _logger = logger;
                _dbContext = dbContext;
            }

            public async Task<Dto> Handle(Command command, CancellationToken cancellationToken)
            {
                _logger.LogInformation("Handler began with: {command}", command.ToJson());

                var space = await _dbContext.Spaces.Where(s => !s.IsOccupied)
                    .OrderBy(s => s.Name).FirstOrDefaultAsync(cancellationToken);

                if (space == null)
                {
                    throw new Exception("no spaces open");
                }

                space.IsOccupied = true;
                space.VehicleId = command.VehicleId;

                await _dbContext.SaveChangesAsync(cancellationToken);

                return new Dto()
                {
                    CreatedUtc = DateTime.UtcNow,
                    ParkingSpotId = space.Id,
                    ParkingSpotName = space.Name,
                    VehicleId = command.VehicleId
                };
            }
        }
    }
}