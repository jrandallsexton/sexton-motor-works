using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Http;

using smw.ai.parking.Data;
using smw.ai.parking.Models;

using System.Collections.Generic;
using System.Linq;

namespace smw.ai.parking
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.ConfigureMediatR();

            services.AddDbContext<ParkingDataContext>(options =>
            {
                options.EnableSensitiveDataLogging();
                options.UseSqlServer(Configuration.GetConnectionString("ParkingDatabase"),
                    b => b.MigrationsAssembly("smw.ai.parking"));
            });

            services.RemoveAll<IHttpMessageHandlerBuilderFilter>(); // prevent unnecessary http logging
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ParkingDataContext dataContext)
        {
            dataContext.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                LoadSeedData(dataContext);
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static void LoadSeedData(ParkingDataContext dataContext)
        {
            if (dataContext.Lots.Any())
                return;

            var lots = GenerateLots(2, 2, 10);
            dataContext.Lots.AddRangeAsync(lots);
            dataContext.SaveChanges();
        }

        private static IEnumerable<ParkingLot> GenerateLots(int lotsToGenerate, int rowPerLow, int spacesPerRow)
        {
            var lots = new List<ParkingLot>();

            for (var i = 0; i < rowPerLow; i++)
            {
                var lotName = $"L{i}";
                lots.Add(new ParkingLot()
                {
                    Name = lotName,
                    Rows = GenerateRows(lotName, rowPerLow, spacesPerRow)
                });
            }

            return lots;
        }

        private static List<ParkingLotRow> GenerateRows(string lotName, int rowsToGenerate, int spacesPerRowToGenerate)
        {
            var rows = new List<ParkingLotRow>();

            for (var i = 0; i < rowsToGenerate; i++)
            {
                var rowName = $"{lotName}R{i}";

                rows.Add(new ParkingLotRow()
                {
                    Name = rowName,
                    Spaces = GenerateSpaces(rowName, spacesPerRowToGenerate)
                });
            }

            return rows;
        }

        private static List<ParkingSpace> GenerateSpaces(string rowName, int numberToGenerate)
        {
            var spaces = new List<ParkingSpace>();

            for (var i = 0; i < numberToGenerate; i++)
            {
                spaces.Add(new ParkingSpace()
                {
                    Name = $"{rowName}S{i}"
                });
            }

            return spaces;
        }
    }
}