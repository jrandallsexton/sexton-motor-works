﻿using FluentAssertions;
using smw.ai.parking.dtos.Commands;
using smw.ai.parking.Features.Parking;
using smw.ai.parking.Models;

using Xunit;

namespace smw.ai.parking.tests.unit.Features.Parking
{
    public class ReserveParkingSpotTests : UnitTestBase
    {
        [Fact]
        public async Task ReserveParkingSpot_TwoSpaces_AllSpacesEmpty_ReturnsFirstSpace()
        {
            // Arrange
            var lot = new ParkingLot()
            {
                Rows = new List<ParkingLotRow>()
                {
                    new()
                    {
                        Name = "L1R1",
                        Spaces = new List<ParkingSpace>()
                        {
                            new()
                            {
                                Name = "L1R1S1"
                            },
                            new()
                            {
                                Name = "L1R1S2"
                            }
                        }
                    }
                }
            };
            await base.DataContext.Lots.AddAsync(lot);
            await base.DataContext.SaveChangesAsync();

            var clientCommand = new ReserveParkingSpotCommand()
            {
                CreatedUtc = DateTime.UtcNow,
                VehicleId = Guid.NewGuid().ToString()[..5]
            };
            var command = new ReserveParkingSpot.Command(clientCommand);

            var handler = Mocker.CreateInstance<ReserveParkingSpot.Handler>();

            // Act
            var result = await handler.Handle(command, CancellationToken.None);

            // Assert
            result.VehicleId.Should().Be(command.VehicleId);
            result.ParkingSpotName.Should().Be("L1R1S1");
        }

        [Fact]
        public async Task ReserveParkingSpot_TwoSpaces_FirstSpaceTaken_ReturnsSecondSpace()
        {
            // Arrange
            var lot = new ParkingLot()
            {
                Rows = new List<ParkingLotRow>()
                {
                    new()
                    {
                        Name = "L1R1",
                        Spaces = new List<ParkingSpace>()
                        {
                            new()
                            {
                                Name = "L1R1S1",
                                IsOccupied = true
                            },
                            new()
                            {
                                Name = "L1R1S2"
                            }
                        }
                    }
                }
            };
            await base.DataContext.Lots.AddAsync(lot);
            await base.DataContext.SaveChangesAsync();

            var clientCommand = new ReserveParkingSpotCommand()
            {
                CreatedUtc = DateTime.UtcNow,
                VehicleId = Guid.NewGuid().ToString()[..5]
            };
            var command = new ReserveParkingSpot.Command(clientCommand);

            var handler = Mocker.CreateInstance<ReserveParkingSpot.Handler>();

            // Act
            var result = await handler.Handle(command, CancellationToken.None);

            // Assert
            result.VehicleId.Should().Be(command.VehicleId);
            result.ParkingSpotName.Should().Be("L1R1S2");
        }
    }
}