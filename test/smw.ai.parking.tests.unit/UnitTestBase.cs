﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

using Moq.AutoMock;

using smw.ai.common.Logging;
using smw.ai.parking.Data;

namespace smw.ai.parking.tests.unit
{
    public abstract class UnitTestBase
    {
        public AutoMocker Mocker { get; }
        public ListLogger Logger { get; }
        public ParkingDataContext DataContext { get; }

        internal UnitTestBase()
        {
            Mocker = new AutoMocker();

            DataContext = new ParkingDataContext(GetDataContextOptions());
            Mocker.Use(typeof(ParkingDataContext), DataContext);

            Logger = CreateLogger(LoggerTypes.List) as ListLogger;

            //var mapperConfig = new MapperConfiguration(c => c.AddProfile(new DynamicMappingProfile()));
            //var mapper = mapperConfig.CreateMapper();
            //Mocker.Use(typeof(IMapper), mapper);
        }

        private static DbContextOptions<ParkingDataContext> GetDataContextOptions()
        {
            // https://stackoverflow.com/questions/52810039/moq-and-setting-up-db-context
            var dbName = Guid.NewGuid().ToString().Substring(0, 5);
            return new DbContextOptionsBuilder<ParkingDataContext>()
                .UseInMemoryDatabase(dbName)
                .Options;
        }

        public static ILogger CreateLogger(LoggerTypes type = LoggerTypes.Null)
        {
            return type == LoggerTypes.List ?
                new ListLogger() :
                NullLoggerFactory.Instance.CreateLogger("Null Logger");
        }
    }
}