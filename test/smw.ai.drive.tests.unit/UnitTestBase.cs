﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

using Moq.AutoMock;

using smw.ai.common.Logging;

namespace smw.ai.drive.tests.unit
{
    public abstract class UnitTestBase
    {
        public AutoMocker Mocker { get; }
        public ListLogger Logger { get; }

        protected UnitTestBase()
        {
            Mocker = new AutoMocker();

            Logger = CreateLogger(LoggerTypes.List) as ListLogger;
        }

        public static ILogger CreateLogger(LoggerTypes type = LoggerTypes.Null)
        {
            return type == LoggerTypes.List ?
                new ListLogger() :
                NullLoggerFactory.Instance.CreateLogger("Null Logger");
        }
    }
}